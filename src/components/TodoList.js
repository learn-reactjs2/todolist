import React, { Component } from 'react'
import TodoItem from './ToDoItem'
import { createStore } from 'redux'

export default class TodoList extends Component {
    constructor(props) {
        super(props);

        this.textInput = React.createRef();
        this.selectControl = React.createRef();

        this.state = {
            todos: []
        }
    }

    add = (e) => {
        e.preventDefault();
        //alert('input: ' + this.textInput.current.value);
        let todo = { id: Date.now(), status: false, value: this.textInput.current.value }
        this.saveLocalTodos(todo)
    }

    componentDidMount() {
        let todos = this.getTodos()

        this.setState({ todos })
        console.log('load')
    }

    saveLocalTodos = (todo) => {
        // Check
        const { todos } = this.state;
        
        todos.push(todo);

        this.setState({ todos })

        localStorage.setItem("todos", JSON.stringify(todos));
    }

    onChangeFilter = (e) => {
        console.log(e.target.value)
        let todos = this.getTodos();
        let newData = []
        switch(e.target.value){
            case 'completed':
                newData = todos.filter(o => o.status === true);
            break;
            
            case 'uncompleted':
                newData = todos.filter(o => o.status === false);
            break;

            default:
                newData = todos;
            break;
        }

        this.setState({ todos: newData })
    }

    getTodos = () => {
        let todos;

        // if it HAS already an item, get it
        if (localStorage.getItem("todos") !== null) {
            todos = JSON.parse(localStorage.getItem("todos"));
        } else {
            todos = [];
        }

        return todos;
    }

    removeLocalTodos = (todo) => {
        const { todos } = this.state;

        let todosNew = todos.filter(p => p.id !== todo.id);

        this.setState({ todos: todosNew });

        localStorage.setItem("todos", JSON.stringify(todosNew));
    }

    updateStatusLocalTodos = (todo) => {
        const { todos } = this.state;

        let todosNew = todos.map(o => {
            if (o.id === todo.id) {
                return { ...o, status: true }
            }
            return o
        })

        this.setState({ todos: todosNew });

        localStorage.setItem("todos", JSON.stringify(todosNew));
    }

    render() {
        const { todos } = this.state;
        
        return (
            <div><form>
                <input type="text" className="todo-input" ref={this.textInput} />
                <button className="todo-button" type="submit" onClick={this.add}>
                    <i className="fas fa-plus-square"></i>
                </button>
                <div className="select">
                    <select name="todos" className="filter-todo" ref={this.selectControl} onChange={this.onChangeFilter}>
                        <option value="all">All</option>
                        <option value="completed">Completed</option>
                        <option value="uncompleted">Uncompleted</option>
                    </select>
                </div>
            </form>
                <div className="todo-container">
                    <ul className="todo-list">
                        {todos.map((data) =>
                            <TodoItem key={data.id.toString()} removeLocalTodos={this.removeLocalTodos} updateStatusLocalTodos={this.updateStatusLocalTodos}
                                data={data} />
                        )}
                    </ul>
                </div>
            </div>);
    }
}