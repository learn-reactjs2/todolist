import React, { Component } from 'react'

export default class TodoItem extends Component {
    updateStatusLocalTodos = (e) => {
        e.preventDefault();

        this.props.updateStatusLocalTodos(this.props.data)
    }

    removeLocalTodos = (e) => {
        e.preventDefault();

        this.props.removeLocalTodos(this.props.data)
    }

    render() {
        const { status, value } = this.props.data;
        let classTodo = 'check-btn '+ (status ? 'completed' : '')
        return (
            <div className='todo'><li className='todo-item' style={{ textDecoration: status ? 'line-through' : '' }}>{value}</li>
                <button className={classTodo} onClick={this.updateStatusLocalTodos}><i className='fas fa-check'></i></button>
                <button className='check-btn' onClick={this.removeLocalTodos}><i className='fas fa-trash'></i></button>
            </div>);
    }
}